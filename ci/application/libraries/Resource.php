<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resource {
    public $SHOP_LIST = array(
        array('id'=>'1', 'name'=>'ASOS'),
        array('id'=>'2', 'name'=>'modcloth'),
        array('id'=>'3', 'name'=>'backcocuntry'),
        array('id'=>'4', 'name'=>'amazon (調整中)'),
        array('id'=>'5', 'name'=>'Disney (商品数を指定して実行)'),
        // array('id'=>'6', 'name'=>'ralphlauren (アクセス不可、作業保留)'),
        array('id'=>'7', 'name'=>'victoriassecret (調整中)'),
        array('id'=>'8', 'name'=>'farfetch'),
        array('id'=>'9', 'name'=>'walmart'),
        array('id'=>'10', 'name'=>'yoox'),
        array('id'=>'11', 'name'=>'society6')
    );
}
?>