<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		define('ID', 1);
	}
	public function index()
	{
		$where = 'status = 1 AND shop_id = '.ID.'';
		$access_url_query = $this->db->where($where)->get('access_urls');
		$this->smarty->assign('access_url', json_decode(json_encode($access_url_query->result()), true));
		$this->smarty->view('asos_view.tpl');
	}
	public function add()
	{
		$this->smarty->view('asos_add.tpl');
	}
	public function add_insert()
	{
		$post = $this->input->post(NULL, TRUE);
		for ($i=0; $i < count($post['memo']); $i++) {
			if (!empty($post['memo'][$i]) && !empty($post['url'][$i])) {
				$data = [
					'memo' => $post['memo'][$i],
					'url' => $post['url'][$i],
					'shop_id' => ID
				];
				$this->db->insert('access_urls', $data);
			}
		}
		redirect('/asos', 'location', 301);
	}
	public function delete()
	{
		$post = $this->input->post(NULL, TRUE);
		$where = 'id = '.$post['id'].' AND shop_id = '.ID.'';
		$this->db->set('status', 2)->where($where)->update('access_urls');
		redirect('/asos', 'location', 301);
	}
	public function edit($shop_id, $id)
	{
		$where = 'shop_id = '.$shop_id.' AND id = '.$id.'';
		$item_querys = $this->db->where($where)->get('access_urls');
		foreach ($item_querys->result() as $row) {
			$item_query['url'] = $row->url;
			$item_query['memo'] = $row->memo;
			$item_query['id'] = $row->id;
		}
		$this->smarty->assign('item_query', json_decode(json_encode($item_query), true));
		$this->smarty->view('asos_edit.tpl');
	}
	public function edit_update()
	{
		$post = $this->input->post(NULL, TRUE);
		$data = [
			'memo' => $post['memo'],
			'url' => $post['url']
		];
		$where = 'id = '.$post['id'].' AND shop_id = '.ID.'';
		$this->db->where($where)->update('access_urls', $data);
		redirect('/asos/edit/'.$post['id'].'/'.ID.'', 'location', 301);
	}
	public function ajax_radiobtn_chk()
	{
		$post = $this->input->post(NULL, TRUE);
		$use_flag_querys = $this->db->where('use_flag', 1)->get('access_urls');
		foreach ($use_flag_querys->result() as $row) {
			$use_flag_query['id'] = $row->id;
		}

		$where_use = 'id = '.$use_flag_query['id'].' AND shop_id = '.ID.'';
		$where_post = 'id = '.$post['id'].' AND shop_id = '.ID.'';
		$this->db->set('use_flag', 2)->where($where_use)->update('access_urls');
		$this->db->set('use_flag', 1)->where($where_post)->update('access_urls');
	}
}