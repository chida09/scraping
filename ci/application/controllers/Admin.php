<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('Resource');
	}
	public function index()
	{
		$this->smarty->assign('shop_list', $this->resource->SHOP_LIST);
		$this->smarty->view('admin.tpl');
	}
	public function admin()
	{
		redirect('/index', 'location', 301);
	}
}