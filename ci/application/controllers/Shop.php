<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('Resource');
	}
	public function view($shop_id)
	{
		$where = 'status = 1 AND shop_id = '.$shop_id.'';
		$access_url_query = $this->db->where($where)->get('access_urls');
		$this->smarty->assign('access_url', json_decode(json_encode($access_url_query->result()), true));
		$this->smarty->assign('shop_id', $shop_id);
		$this->smarty->assign('shop_list', $this->resource->SHOP_LIST);
		$this->smarty->view('shop_view.tpl');
	}
	public function add($shop_id)
	{
		$this->smarty->assign('shop_id', $shop_id);
		$this->smarty->assign('shop_list', $this->resource->SHOP_LIST);
		$this->smarty->view('shop_add.tpl');
	}
	public function add_insert($shop_id)
	{
		$post = $this->input->post(NULL, TRUE);
		for ($i=0; $i < count($post['memo']); $i++) {
			if (!empty($post['memo'][$i]) && !empty($post['url'][$i])) {
				$data = [
					'memo' => $post['memo'][$i],
					'url' => $post['url'][$i],
					'shop_id' => $shop_id
				];
				$this->db->insert('access_urls', $data);
			}
		}
		redirect('/shop/view/'.$shop_id.'', 'location', 301);
	}
	public function delete($shop_id)
	{
		$post = $this->input->post(NULL, TRUE);
		$where = 'id = '.$post['id'].' AND shop_id = '.$shop_id.'';
		$this->db->set('status', 2)->where($where)->update('access_urls');
		redirect('/shop/view/'.$shop_id.'', 'location', 301);
	}
	public function edit($shop_id, $id)
	{
		$item_query = null;
		$where = 'shop_id = '.$shop_id.' AND id = '.$id.'';
		$item_querys = $this->db->where($where)->get('access_urls');
		foreach ($item_querys->result() as $row) {
			$item_query['url'] = $row->url;
			$item_query['memo'] = $row->memo;
			$item_query['id'] = $row->id;
		}
		$this->smarty->assign('item_query', json_decode(json_encode($item_query), true));
		$this->smarty->assign('shop_id', $shop_id);
		$this->smarty->view('shop_edit.tpl');
	}
	public function edit_update($shop_id)
	{
		$post = $this->input->post(NULL, TRUE);
		$data = [
			'memo' => $post['memo'],
			'url' => $post['url']
		];
		$where = 'id = '.$post['id'].' AND shop_id = '.$shop_id.'';
		$this->db->where($where)->update('access_urls', $data);
		redirect('/shop/edit/'.$shop_id.'/'.$post['id'].'', 'location', 301);
	}
	public function ajax_radiobtn_chk($shop_id)
	{
		$post = $this->input->post(NULL, TRUE);

		$where = 'use_flag = 1 AND shop_id = '.$shop_id.'';
		$use_flag_querys = $this->db->where($where)->get('access_urls');
		foreach ($use_flag_querys->result() as $row) {
			$use_flag_query = $row->id;
		}

		$where_use = 'id = '.$use_flag_query.' AND shop_id = '.$shop_id.'';
		$this->db->set('use_flag', 2)->where($where_use)->update('access_urls');

		$where_post = 'id = '.$post['id'].' AND shop_id = '.$shop_id.'';
		$this->db->set('use_flag', 1)->where($where_post)->update('access_urls');
	}
}