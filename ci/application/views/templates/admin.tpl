<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <h2>サイト一覧</h2>
        <div>
            <ul>
                <li>システム実行時の指定数は、ページ数でお願いします。</li>
                {foreach from=$shop_list item=i}
                <li><a href="/shop/view/{$i.id}" >{$i.name}</a></li>
                {/foreach}
            </ul>
        </div>
    </div>
</body>
</html>