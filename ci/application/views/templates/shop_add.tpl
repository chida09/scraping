<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
        {foreach from=$shop_list item=i}
            {if $i.id == $shop_id}
                <h2>{$i.name}&nbsp;追加</h2>
            {/if}
        {/foreach}

        <div>
            <form action="/shop/add_insert/{$shop_id}" method="post">
                <ul>
                    <li><label for="memo1"><span>タイトル</span><input type="text" name="memo[]" id="memo1" class="add_memo"></label></li>
                    <li><label for="url1"><span>URL</span><input type="text" name="url[]" id="url1" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo2"><span>タイトル</span><input type="text" name="memo[]" id="memo2" class="add_memo"></label></li>
                    <li><label for="url2"><span>URL</span><input type="text" name="url[]" id="url2" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo3"><span>タイトル</span><input type="text" name="memo[]" id="memo3" class="add_memo"></label></li>
                    <li><label for="url3"><span>URL</span><input type="text" name="url[]" id="url3" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo4"><span>タイトル</span><input type="text" name="memo[]" id="memo4" class="add_memo"></label></li>
                    <li><label for="url4"><span>URL</span><input type="text" name="url[]" id="url4" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo5"><span>タイトル</span><input type="text" name="memo[]" id="memo5" class="add_memo"></label></li>
                    <li><label for="url5"><span>URL</span><input type="text" name="url[]" id="url5" class="add_text"></label></li>
                </ul>
                <p><input class="btn btn_blue" type="submit" value="送 信">&nbsp;&nbsp;<a class="btn btn_black" href="/shop/view/{$shop_id}">戻 る</a></p>
            </form>
        </div>
    </div>
</body>
</html>