<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex,nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
        {foreach from=$shop_list item=i}
        {if $i.id == $shop_id}<h2>{$i.name}</h2>{/if}
        {/foreach}
        <a href="/">TOPに戻る</a>&nbsp;/&nbsp;
        <a href="/shop/add/{$shop_id}">URL追加</a>&nbsp;/&nbsp;
        <a href="/nextcloud/index.php/apps/files/?dir=/&fileid=7" target="_blank">Nextcloud</a>&nbsp;/&nbsp;
        <a href="https://docs.google.com/document/d/14Ne9OSAL4KYE76pge0cOfiMrteULsVHpOi-tXsbYqI4/edit" target="_blank">システムの使い方</a>

        {if $shop_id == 9}
        <ul>
            <li>walmartはステータスを有効する前に、リンクが以下のように商品一覧になっているかご確認をお願いします。</li>
            <li>・<a href="https://www.walmart.com/browse/home/blankets/4044_539103_4756">Blankets & Throws</a></li>
            <li>・<a href="https://www.walmart.com/browse/clothing/5438?facet=special_offers%3AClearance%7C%7Cspecial_offers%3AReduced+Price%7C%7Cspecial_offers%3ARollback%7C%7Cspecial_offers%3ASpecial+Buy&page=1&sort=new">Clothing & Accessories</a></li>
        </ul>
        {/if}

        <div>
            <table>
                <tbody>
                    <tr>
                        <th>No</th>
                        <th>ステータス</th>
                        <th>取得対象</th>
                        <th colspan="2">設定</th>
                    </tr>
                    {foreach from=$access_url key=k item=i}
                    <tr>
                        <td>{$k+1}</td>
                        <td>
                            <input name="use_flag" type="radio" id="radio_{$k+1}" value="{$i.id}" {if $i.use_flag == 1}checked{/if}>&nbsp;<label for="radio_{$k+1}">有効</label>
                        </td>
                        <td><a href="{$i.url}" target="_blank">{$i.memo}</a></td>
                        <td><a href="/shop/edit/{$shop_id}/{$i.id}" class="btn btn_green">編 集</a></td>
                        <td>
                            <form action="/shop/delete/{$shop_id}" method="POST">
                                <input class="btn btn_red" type="submit" value="削 除">
                                <input type="hidden" name="id" value="{$i.id}">
                            </form>
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            <input type="hidden" name="shop_id" value="{$shop_id}">
        </div>
    </div>
</body>
</html>

{literal}
<script>
$(function(){
    $('input[name="use_flag"]').on('click', function() {
        if ($(this).prop('checked')){
            $(this).prop('checked', true);
        }
        const id = $("input[name='use_flag']:checked").val();
        const shop_id = $("input[name='shop_id']").val();

        $.ajax({
            url: 'http://153.126.176.201/shop/ajax_radiobtn_chk/' + shop_id,
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
            }
        })
    });
});
</script>
{/literal}