<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <h2>編集</h2>
        <div>
            <form action="/shop/edit_update/{$shop_id}" method="post">
                <p><label for="memo">タイトル<br><input type="text" name="memo" id="memo" value="{$item_query.memo}"><br></label></p>
                <p><label for="url">URL<br><input type="text" name="url" id="url" value="{$item_query.url}" style="width: 80%;"></label></p>
                <p><input class="btn btn_blue" type="submit" value="保 存">&nbsp;&nbsp;<a class="btn btn_black" href="/shop/view/{$shop_id}">戻 る</a></p>
                <input type="hidden" name="id" value="{$item_query.id}">
            </form>
        </div>
    </div>
</body>
</html>