<?php
/* Smarty version 3.1.34-dev-7, created on 2019-01-19 05:06:44
  from '/var/app/ci/application/views/templates/admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c42b064800490_62604287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '169bd9450e560951b7556c783cf571f293e8c02b' => 
    array (
      0 => '/var/app/ci/application/views/templates/admin.tpl',
      1 => 1547874402,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c42b064800490_62604287 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <h2>サイト一覧</h2>
        <div>
            <ul>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shop_list']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
                <li><a href="/shop/view/<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" ><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</a></li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
        </div>
    </div>
</body>
</html><?php }
}
