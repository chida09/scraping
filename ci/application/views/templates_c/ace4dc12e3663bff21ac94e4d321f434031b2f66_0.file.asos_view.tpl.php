<?php
/* Smarty version 3.1.34-dev-7, created on 2019-01-19 02:03:47
  from '/var/app/ci/application/views/templates/asos_view.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c428583970304_76589283',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ace4dc12e3663bff21ac94e4d321f434031b2f66' => 
    array (
      0 => '/var/app/ci/application/views/templates/asos_view.tpl',
      1 => 1547863425,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c428583970304_76589283 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex,nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ASOS | 管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
        <h2>ASOS&nbsp;設定</h2>
        <a href="/">TOPに戻る</a>&nbsp;/&nbsp;
        <a href="/asos/add">URL追加</a>&nbsp;/&nbsp;
        <a href="/nextcloud/index.php/apps/files/?dir=/&fileid=7" target="_blank">Nextcloud</a>&nbsp;/&nbsp;
        <a href="https://docs.google.com/spreadsheets/d/1FoKilfWBpwXdq8Rf8X7hrUfOi1K-cAmsQk5QJimdnJU/edit#gid=0" target="_blank">CSV設定マニュアル</a>
        <div>
            <table>
                <tbody>
                    <tr>
                        <th>No</th>
                        <th>ステータス</th>
                        <th>URL</th>
                        <th colspan="2">設定</th>
                    </tr>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['access_url']->value, 'i', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['i']->value) {
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                        <td>
                            <input name="use_flag" type="radio" id="radio_<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['i']->value['use_flag'] == 1) {?>checked<?php }?>>&nbsp;<label for="radio_<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">有効</label>
                        </td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['i']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['memo'];?>
</a></td>
                        <td><a href="/asos/edit/1/<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" class="btn btn_green">編 集</a></td>
                        <td>
                            <form action="/asos/delete" method="POST">
                                <input class="btn btn_red" type="submit" value="削 除">
                                <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
">
                            </form>
                        </td>
                    </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>


<?php echo '<script'; ?>
>
$(function(){
    $('input[name="use_flag"]').on('click', function() {
        if ($(this).prop('checked')){
            $(this).prop('checked', true);
        }
        const id = $("input[name='use_flag']:checked").val();
        $.ajax({
            url: 'http://153.126.176.201/asos/ajax_radiobtn_chk',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
            }
        })
    });
});
<?php echo '</script'; ?>
>
<?php }
}
