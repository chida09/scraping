<?php
/* Smarty version 3.1.34-dev-7, created on 2019-01-19 02:43:22
  from '/var/app/ci/application/views/templates/shop_edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c428eca92d3e1_46061027',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad394435bd9cb4e341a97b4263f1ec23a0a68230' => 
    array (
      0 => '/var/app/ci/application/views/templates/shop_edit.tpl',
      1 => 1547865798,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c428eca92d3e1_46061027 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <h2>編集</h2>
        <div>
            <form action="/shop/edit_update/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
" method="post">
                <p><label for="memo">タイトル<br><input type="text" name="memo" id="memo" value="<?php echo $_smarty_tpl->tpl_vars['item_query']->value['memo'];?>
"><br></label></p>
                <p><label for="url">URL<br><input type="text" name="url" id="url" value="<?php echo $_smarty_tpl->tpl_vars['item_query']->value['url'];?>
" style="width: 80%;"></label></p>
                <p><input class="btn btn_blue" type="submit" value="保 存">&nbsp;&nbsp;<a class="btn btn_black" href="/shop/view/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">戻 る</a></p>
                <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['item_query']->value['id'];?>
">
            </form>
        </div>
    </div>
</body>
</html><?php }
}
