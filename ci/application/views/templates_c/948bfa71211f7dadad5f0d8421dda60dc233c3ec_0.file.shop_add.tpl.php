<?php
/* Smarty version 3.1.34-dev-7, created on 2019-01-19 04:48:36
  from '/var/app/ci/application/views/templates/shop_add.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c42ac249454d4_25711723',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '948bfa71211f7dadad5f0d8421dda60dc233c3ec' => 
    array (
      0 => '/var/app/ci/application/views/templates/shop_add.tpl',
      1 => 1547873310,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c42ac249454d4_25711723 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shop_list']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['i']->value['id'] == $_smarty_tpl->tpl_vars['shop_id']->value) {?>
                <h2><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
&nbsp;追加</h2>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

        <div>
            <form action="/shop/add_insert/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
" method="post">
                <ul>
                    <li><label for="memo1"><span>タイトル</span><input type="text" name="memo[]" id="memo1" class="add_memo"></label></li>
                    <li><label for="url1"><span>URL</span><input type="text" name="url[]" id="url1" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo2"><span>タイトル</span><input type="text" name="memo[]" id="memo2" class="add_memo"></label></li>
                    <li><label for="url2"><span>URL</span><input type="text" name="url[]" id="url2" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo3"><span>タイトル</span><input type="text" name="memo[]" id="memo3" class="add_memo"></label></li>
                    <li><label for="url3"><span>URL</span><input type="text" name="url[]" id="url3" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo4"><span>タイトル</span><input type="text" name="memo[]" id="memo4" class="add_memo"></label></li>
                    <li><label for="url4"><span>URL</span><input type="text" name="url[]" id="url4" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo5"><span>タイトル</span><input type="text" name="memo[]" id="memo5" class="add_memo"></label></li>
                    <li><label for="url5"><span>URL</span><input type="text" name="url[]" id="url5" class="add_text"></label></li>
                </ul>
                <p><input class="btn btn_blue" type="submit" value="送 信">&nbsp;&nbsp;<a class="btn btn_black" href="/shop/view/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">戻 る</a></p>
            </form>
        </div>
    </div>
</body>
</html><?php }
}
