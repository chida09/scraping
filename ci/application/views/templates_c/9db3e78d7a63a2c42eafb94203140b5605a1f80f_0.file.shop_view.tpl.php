<?php
/* Smarty version 3.1.34-dev-7, created on 2019-01-19 05:10:32
  from '/var/app/ci/application/views/templates/shop_view.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c42b1483a1de4_62776258',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9db3e78d7a63a2c42eafb94203140b5605a1f80f' => 
    array (
      0 => '/var/app/ci/application/views/templates/shop_view.tpl',
      1 => 1547874630,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c42b1483a1de4_62776258 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex,nofollow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shop_list']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['i']->value['id'] == $_smarty_tpl->tpl_vars['shop_id']->value) {?>
                <h2><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
&nbsp;設定</h2>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

        <a href="/">TOPに戻る</a>&nbsp;/&nbsp;
        <a href="/shop/add/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">URL追加</a>&nbsp;/&nbsp;
        <a href="/nextcloud/index.php/apps/files/?dir=/&fileid=7" target="_blank">Nextcloud</a>
        <?php if ($_smarty_tpl->tpl_vars['shop_id']->value == 1) {?>&nbsp;/&nbsp;<a href="https://docs.google.com/spreadsheets/d/1FoKilfWBpwXdq8Rf8X7hrUfOi1K-cAmsQk5QJimdnJU/edit#gid=0" target="_blank">CSV設定マニュアル</a><?php }?>
        <div>
            <table>
                <tbody>
                    <tr>
                        <th>No</th>
                        <th>ステータス</th>
                        <th>取得対象</th>
                        <th colspan="2">設定</th>
                    </tr>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['access_url']->value, 'i', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['i']->value) {
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                        <td>
                            <input name="use_flag" type="radio" id="radio_<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['i']->value['use_flag'] == 1) {?>checked<?php }?>>&nbsp;<label for="radio_<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">有効</label>
                        </td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['i']->value['url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['i']->value['memo'];?>
</a></td>
                        <td><a href="/shop/edit/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
" class="btn btn_green">編 集</a></td>
                        <td>
                            <form action="/shop/delete/<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
" method="POST">
                                <input class="btn btn_red" type="submit" value="削 除">
                                <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['i']->value['id'];?>
">
                            </form>
                        </td>
                    </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </tbody>
            </table>
            <input type="hidden" name="shop_id" value="<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
">
        </div>
    </div>
</body>
</html>


<?php echo '<script'; ?>
>
$(function(){
    $('input[name="use_flag"]').on('click', function() {
        if ($(this).prop('checked')){
            $(this).prop('checked', true);
        }
        const id = $("input[name='use_flag']:checked").val();
        const shop_id = $("input[name='shop_id']").val();

        $.ajax({
            url: 'http://153.126.176.201/shop/ajax_radiobtn_chk/' + shop_id,
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
            }
        })
    });
});
<?php echo '</script'; ?>
>
<?php }
}
