<?php
/* Smarty version 3.1.34-dev-7, created on 2018-12-31 10:02:37
  from '/var/app/ci/application/views/templates/add.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5c29e93d1dce95_66911824',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cb58c93599f1add3963da1fbdd7c9b97bb7a8290' => 
    array (
      0 => '/var/app/ci/application/views/templates/add.tpl',
      1 => 1546250556,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c29e93d1dce95_66911824 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ASOS | 管理画面</title>
    <?php echo '<script'; ?>
 src="/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <link href="/css/common.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <h2>ASOS&nbsp;追加</h2>
        <div>
            <form action="/admin/add_insert" method="post">
                <ul>
                    <li><label for="memo1"><span>タイトル</span><input type="text" name="memo[]" id="memo1" class="add_memo"></label></li>
                    <li><label for="url1"><span>URL</span><input type="text" name="url[]" id="url1" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo2"><span>タイトル</span><input type="text" name="memo[]" id="memo2" class="add_memo"></label></li>
                    <li><label for="url2"><span>URL</span><input type="text" name="url[]" id="url2" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo3"><span>タイトル</span><input type="text" name="memo[]" id="memo3" class="add_memo"></label></li>
                    <li><label for="url3"><span>URL</span><input type="text" name="url[]" id="url3" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo4"><span>タイトル</span><input type="text" name="memo[]" id="memo4" class="add_memo"></label></li>
                    <li><label for="url4"><span>URL</span><input type="text" name="url[]" id="url4" class="add_text"></label></li>
                </ul>
                <ul>
                    <li><label for="memo5"><span>タイトル</span><input type="text" name="memo[]" id="memo5" class="add_memo"></label></li>
                    <li><label for="url5"><span>URL</span><input type="text" name="url[]" id="url5" class="add_text"></label></li>
                </ul>
                <p><input class="btn btn_blue" type="submit" value="送 信">&nbsp;&nbsp;<a class="btn btn_black" href="/admin">戻 る</a></p>
            </form>
        </div>
    </div>
</body>
</html><?php }
}
