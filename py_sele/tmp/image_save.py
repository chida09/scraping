from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import urllib.error
import urllib.request
import os


options = Options()
options.add_argument('--disable-gpu')
options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
driver = webdriver.Chrome(chrome_options=options)

target_url = 'https://www.asos.com/asos-design/asos-design-cord-jacket-with-borg-collar-in-rust/prd/9492361?clr=rust-brown&SearchQuery=&cid=2110&gridcolumn=1&gridrow=1&gridsize=3&pge=1&pgesize=72&totalstyles=2110'

driver.get(target_url)
time.sleep(1)
driver.execute_script("$('.gallery-image').css('visibility', '')")
image_src = driver.find_elements_by_css_selector('div.window > ul > li > img')
image_src.pop(0)
product_code = '1252262'
for i, value in enumerate(image_src):
    image_name = '/var/app/ci/public/nextcloud/data/nextuser/files/Photos/' + str(product_code) + '_' + str(i) + '.jpeg'
    try:
        data = urllib.request.urlopen(value.get_attribute('src')).read()
        with open(image_name, mode='wb') as f:
            f.write(data)
    except urllib.error.URLError as e:
        print(e)

os.system('cd /var/app/ci/public/nextcloud && sudo -u apache php occ files:scan nextuser')
driver.close()