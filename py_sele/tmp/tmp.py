from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime
import MySQLdb
import time
import sys
import csv
import re
import urllib.error
import urllib.request
import os
import math
import random
import buyma_actions


class SeleExec():
    def __init__(self):
        options = Options()
        options.add_argument('--disable-gpu')
        options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
        # options.add_argument('--user-data-dir=' + os.getcwd() + '/data/buyma_chida')
        # options.add_argument('--password-store=detect')
        self.driver = webdriver.Chrome(chrome_options=options)
        self.connection = MySQLdb.connect(user='root', passwd='L3,g6ogp8>52l=K', host='localhost', db='main', charset='utf8')
        self.cursor = self.connection.cursor()
        self.argv = sys.argv

    def __del__(self):
        self.driver.close()
        self.cursor.close
        self.connection.close

    def argv_chk(self):
        if len(self.argv) != 2:
            print('ファイル名を指定してください。')
            sys.exit()

    def sele_run(self):
        driver = self.driver
        file_name = self.argv
        file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
        actions = ActionChains(driver)

        try:
            driver.get('https://www.buyma.com/my/sell/new/')
            time.sleep(1)

            # utf8,カンマ区切りのcsvを読込
            os.system('sudo chmod -R 777 /var/app/ci/public/nextcloud/*')
            with open(file_path + 'Documents/' + file_name[1] + '.csv', 'r') as f:
                reader = csv.reader(f)
                header = next(reader) # ヘッダーを読み飛ばす

                for row in reader:


        except Exception as e:
            print('エラーが発生しました。')
            start.exec_error(e)

    def exec_error(self, e):
        driver = self.driver
        print(e)
        print(driver.current_url)
        driver.get_screenshot_as_file('/tmp/asos2_error_' + datetime.now().strftime("%Y-%m-%d %H:%i:%s") + '.png')

if __name__ == "__main__":
    start = SeleExec()
    start.argv_chk()
    start.sele_run()