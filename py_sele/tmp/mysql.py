import MySQLdb

def db_sample():

    # 接続する
    connection = MySQLdb.connect(
            user='root',
            passwd='L3,g6ogp8>52l=K',
            host='localhost',
            db='main',
            charset='utf8'
        )

    # カーソルを取得する
    cursor = connection.cursor()

    # クエリを実行する
    sql = "select * from asos_access_urls limit 3"
    cursor.execute(sql)

    # 実行結果をすべて取得する
    rows = cursor.fetchall()

    # 一行ずつ表示する
    for row in rows:
        print(row)

    cursor.close
    connection.close

if __name__ == "__main__":
    db_sample()