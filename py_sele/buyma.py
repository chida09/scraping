from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from datetime import datetime
import MySQLdb
import time
import sys
import csv
import re
import urllib.error
import urllib.request
import os
import math
import random
import buyma_actions


class SeleExec():
    def __init__(self):
        options = Options()
        # options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
        options.add_argument('--user-data-dir=' + os.getcwd() + '/data/buyma_chida')
        options.add_argument('--password-store=detect')
        self.driver = webdriver.Chrome(chrome_options=options)
        self.connection = MySQLdb.connect(user='root', passwd='L3,g6ogp8>52l=K', host='localhost', db='main', charset='utf8')
        self.cursor = self.connection.cursor()
        self.argv = sys.argv

    def __del__(self):
        self.driver.close()
        self.cursor.close
        self.connection.close

    def argv_chk(self):
        if len(self.argv) != 2:
            print('ファイル名を指定してください。')
            sys.exit()

    def sele_run(self):
        driver = self.driver
        file_name = self.argv
        file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
        actions = ActionChains(driver)

        try:
            # utf8,カンマ区切りのcsvを読込
            os.system('sudo chmod -R 777 /var/app/ci/public/nextcloud/*')
            with open(file_path + 'Documents/' + file_name[1] + '.csv', 'r') as f:
                reader = csv.reader(f)
                header = next(reader) # ヘッダーを読み飛ばす

                for row in reader:
                    driver.get('http://www.buyma.com/my/sell/new/')
                    time.sleep(2)

                    driver.execute_script('document.querySelectorAll("#react-select-3--value-item")[0].textContent = "2019 SS";')
                    time.sleep(1000)
                    driver.execute_script("$('input').css('display','')") # 画像 MAX4枚(必須)
                    for i in range(0, 4):
                        time.sleep(1)
                        driver.find_element_by_css_selector("input[type='file']").send_keys(file_path + 'Photos/' + row[0] + '_' + str(i) +'.jpeg')
                        driver.implicitly_wait(1.5)

                    time.sleep(1)
                    driver.find_element_by_css_selector('.bmm-c-field__input > input').send_keys(row[1]) # 商品名(必須)
                    driver.implicitly_wait(1.5)

                    time.sleep(1)
                    driver.find_element_by_css_selector('.bmm-c-field__input > textarea').send_keys(row[3]) # 商品コメント(必須)
                    driver.implicitly_wait(1.5)

                    start.data_selected(row, num=1) # カテゴリ1(必須)
                    time.sleep(1)
                    start.data_selected(row, num=2) # カテゴリ2(必須)
                    time.sleep(1)
                    start.data_selected(row, num=3) # カテゴリ3(必須)

                    time.sleep(1)
                    driver.execute_script('document.querySelectorAll(".col-9 > div > div > div > div > div > div.grid-noBottom > div > div > div > div > input")[0].value = "ASOS";') # ブランド

                    driver.find_element_by_css_selector('.bmm-c-field__input > textarea').click()

                    try:
                        chk = row[7]
                        time.sleep(1)
                        start.data_selected(row, num=4) # シーズン
                    except Exception as e:
                        print("シーズン未選択")

                    try:
                        chk = row[8]
                        time.sleep(1)
                        start.data_selected(row, num=5) # テーマ
                    except Exception as e:
                        print("テーマ未選択")

                    start.data_selected(row, num=6) # 色の系統1(必須)
                    driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr > td:nth-child(3) > div > div > input').send_keys(row[10]) # 色名1(必須)

                    try:
                        chk = row[11]
                        start.data_selected(row, num=7) # 色の系統2
                        time.sleep(1)
                        driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > input').send_keys(row[12]) # 色名2
                    except Exception as e:
                        print("色の系統2未選択")

                    try:
                        chk = row[13]
                        start.data_selected(row, num=8) # 色の系統3
                        driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > input').send_keys(row[14]) # 色名3
                    except Exception as e:
                        print("色の系統3未選択")

                    start.data_selected(row, num=9) # サイズ(必須)
                    if row[15] == 'バリエーションなし':
                        print('バリエーションなし')
                        time.sleep(1)
                        start.data_selected(row, num=18)
                        start.data_selected(row, num=11) # 日本参考サイズ1
                        start.variation_size(row, num=1)
                    else:
                        # バリエーションあり
                        time.sleep(1)
                        start.data_selected(row, num=10) # バリエーション サイズ選択1
                        driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr > td:nth-child(2) > div > div > div > input').send_keys(row[18]) # サイズ名1
                        start.data_selected(row, num=11) # 日本参考サイズ1
                        start.variation_size(row, num=1)

                        time.sleep(1)
                        driver.find_element_by_link_text('新しいサイズを追加').click()
                        driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > input').send_keys(row[26]) # サイズ名2
                        start.data_selected(row, num=12) # 日本参考サイズ2
                        start.variation_size(row, num=2)

                        time.sleep(1)
                        driver.find_element_by_link_text('新しいサイズを追加').click()
                        driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > input').send_keys(row[34]) # サイズ名3
                        start.data_selected(row, num=13) # 日本参考サイズ3
                        start.variation_size(row, num=3)

                    driver.find_element_by_css_selector('.col-9 > div > div > div.bmm-c-field > div.bmm-c-field__input > textarea').send_keys(row[42]) # 色・sサイズ補足情報
                    driver.implicitly_wait(1.5)

                    time.sleep(1)



                    driver.execute_script("$(\"input[type='checkbox']\").css('display','block')") # 販売可否/在庫(必須)
                    driver.find_element_by_css_selector('.col-9 > div > div > div > label > input').location_once_scrolled_into_view

                    # 在庫
                    for i in range(1, 10):
                        try:
                            if row[42 + int(i)] == 'なし':
                                driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(' + str(i) + ') > td:nth-child(3) > label').click()
                        except Exception as e:
                            print("")

                    try:
                        driver.find_element_by_css_selector('.col-9 > div > div > div:nth-child(2) > div > div.grid-noBottom > div > div > div > div > input').send_keys(row[52]) # 数量
                    except Exception as e:
                        print("")

                    try:
                        if row[53] == "なし":
                            driver.find_element_by_css_selector('.col-9 > div > div > div > label > input').click() # 連絡事項
                    except Exception as e:
                        print("")

                    time.sleep(1)
                    driver.find_element_by_css_selector('.react-datepicker-wrapper > div > input').location_once_scrolled_into_view

                    # @todo csvから読み込んだ日付データが正しく入力出来ない
                    # driver.execute_script('document.querySelectorAll(".col-9 > div > div > div > div > div > div > div > div.react-datepicker-wrapper > div > input")[0].value = '  + str(row[55]) + ';') # 購入期限

                    driver.execute_script("$(\"input[type='radio']\").css('display','block')") # 買付地
                    driver.find_element_by_css_selector('.col-9 > div > div:nth-child(1) > div > label:nth-child(2) > input').location_once_scrolled_into_view
                    driver.execute_script("scrollBy(0, 500)")
                    driver.implicitly_wait(1.5)
                    time.sleep(1)
                    driver.execute_script("$(\"input[type='radio']\").css('display','block')") # 買付地
                    driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[1].click();") # 買付地/海外で固定
                    start.data_selected(row, num=14)
                    start.data_selected(row, num=15)
                    try:
                        chk = row[37] # 配列が空かチェック。空なら入力しない
                        start.data_selected(row, num=16)
                    except Exception as e:
                        print("")

                    driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[2].click();") # 発送地/国内で固定
                    start.data_selected(row, num=17) # 発送地/神奈川県で固定

                    time.sleep(1)
                    driver.find_element_by_css_selector('.bmm-c-field__input > div > div.col-4 > div > div > input').send_keys(row[59]) # 価格

                    try:
                        chk = row[60] # 配列が空かチェック。空なら入力しない
                        print("row[60]")
                        driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[5].click();") # 参考価格
                        time.sleep(1)
                        driver.find_element_by_css_selector('.col-9 > div > div > div:nth-child(2) > div > div > div > div > input').send_keys(row[60])
                    except Exception as e:
                        print("")

                    try:
                        time.sleep(1)
                        driver.find_element_by_css_selector('form > div:nth-child(8) > div > div > div.col-9 > div > div > div.bmm-c-field__input > input').send_keys(row[61]) # 出品メモ
                    except Exception as e:
                        print("")

                    # driver.execute_script('document.querySelectorAll(".bmm-l-cnt__body.sell-body > form > div.sell-btnbar > div > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--m.bmm-c-btn--thick")[0].click();')
                    # time.sleep(1000)

                    # driver.execute_script("window.open()")
                    # new_window = driver.window_handles
                    # driver.switch_to.window(new_window[1])

        except Exception as e:
            print('エラーが発生しました。')
            start.exec_error(e)
            time.sleep(1000)

    def exec_error(self, e):
        driver = self.driver
        print(e)
        print(driver.current_url)
        driver.get_screenshot_as_file('/tmp/asos2_error_' + datetime.now().strftime("%Y-%m-%d %H:%i:%s") + '.png')

    # サイズ選択
    def variation_size(self, row, num):
        driver = self.driver
        if num == 1:
            try:
                driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr > td:nth-child(4) > div > p > a').click() # 有,編集
            except Exception as e:
                driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr > td:nth-child(3) > div > p > a').click() # 無,編集

            for i in range(1, 7):
                try:
                    # 着丈20, 肩幅21, 胸囲22, 袖丈23, ウエスト24, ヒップ25
                    driver.find_element_by_css_selector('.sell-size-detail-form > div:nth-child(' + str(i) + ') > div > input').send_keys(row[19 + int(i)])
                except Exception as e:
                    print("")
        elif num == 2:
            driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > p > a').click() # 編集
            for i in range(1, 7):
                try:
                    # 着丈29, 肩幅30, 胸囲31, 袖丈32, ウエスト33, ヒップ34
                    driver.find_element_by_css_selector('.sell-size-detail-form > div:nth-child(' + str(i) + ') > div > input').send_keys(row[27 + int(i)])
                except Exception as e:
                    print("")
        else:
            driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > p > a').click() # 編集
            for i in range(1, 7):
                try:
                    # 着丈36, 肩幅37, 胸囲38, 袖丈39, ウエスト40, ヒップ41
                    driver.find_element_by_css_selector('.sell-size-detail-form > div:nth-child(' + str(i) + ') > div > input').send_keys(row[35 + int(i)])
                except Exception as e:
                    print("")
        driver.find_element_by_css_selector('.bmm-c-modal__btns > button.bmm-c-btn.bmm-c-btn--p').click() # 設定完了

    # セレクトボックス処理
    def data_selected(self, row, num):
        driver = self.driver
        item_ar = ""
        item_text = ""
        driver.implicitly_wait(1.5)

        if int(num) <= 3: # カテゴリ
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.SPACE).perform()
            if int(num) == 1:
                item_chk = row[4]
            elif int(num) == 2:
                item_chk = row[5]
            elif int(num) == 3:
                item_chk = row[6]
            else:
                item_chk = "カテゴリ選択なし"

            time.sleep(1.5)
            item_text = driver.find_element_by_css_selector('.Select-menu-outer').text
        elif int(num) == 4: # シーズン
            item_chk = row[7]
            driver.find_element_by_link_text('「シーズン」を設定しよう').send_keys(Keys.TAB)
            ActionChains(driver).send_keys(Keys.SPACE).perform()
        elif int(num) == 5: # テーマ
            item_chk = row[8]
            driver.find_element_by_link_text('「シーズン」を設定しよう').send_keys(Keys.TAB)
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.SPACE).perform()
            time.sleep(1.5)
            item_text = driver.find_element_by_css_selector('.Select-menu-outer').text
        elif int(num) == 6: # 色の系統1
            item_chk = row[9]
            driver.find_element_by_link_text('色サイズの設定方法').send_keys(Keys.TAB)
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.DOWN).perform()
        elif int(num) == 7: # 色の系統2
            item_chk = row[11]
            driver.find_element_by_css_selector('.bmm-c-form-table__foot > a').click()
            driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > input').click()
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.DOWN).perform()
        elif int(num) == 8: # 色の系統3
            item_chk = row[13]
            driver.find_element_by_css_selector('.bmm-c-form-table__foot > a').click()
            driver.find_element_by_css_selector('.bmm-c-form-table__body > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > input').click()
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.DOWN).perform()
        elif int(num) == 9: # サイズ
            item_chk = row[15]
            driver.find_element_by_link_text('過去の色・サイズからコピー').send_keys(Keys.SHIFT, Keys.TAB)
            ActionChains(driver).send_keys(Keys.ENTER).send_keys(Keys.RIGHT).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 10: # バリエーション サイズ選択1
            item_chk = row[17]
            driver.find_element_by_link_text('色サイズの設定方法').send_keys(Keys.TAB)
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 11: # 参考日本サイズ1
            item_chk = row[19]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 12: # 参考日本サイズ2
            item_chk = row[27]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 13: # 参考日本サイズ3
            item_chk = row[35]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 14: # 買い付け地
            item_chk = row[56]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 15: # 買い付け地2
            item_chk = row[57]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 16: # 買い付け地3
            item_chk = row[58]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 17: # 発送地
            item_chk = "神奈川県"
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        elif int(num) == 18: # サイズ選択 バリエーションなし
            item_chk = row[16]
            ActionChains(driver).send_keys(Keys.TAB).send_keys(Keys.ENTER).perform()
        else:
            print('未入力')

        # セレクトボックスの値を取得
        driver.implicitly_wait(1.5)
        if int(num) <= 3:
            item_ar = driver.find_element_by_css_selector('.Select-menu-outer').text.split()
            if '第一カテゴリから選択' in item_text:
                item_ar.remove('第一カテゴリから選択')
        elif int(num) == 5:
            item_ar_tmp = driver.find_element_by_css_selector('.Select-menu-outer').text.splitlines()
            if 'PUSH' in item_text:
                item_ar_tmp.remove('PUSH')
            item_ar = item_ar_tmp[0::2]
        else:
            item_ar = driver.find_element_by_css_selector('.Select-menu-outer').text.splitlines()

        # セレクトボックスの値を選択
        tab_action_cnt = ""
        for i, item_tmp in enumerate(item_ar):
            try:
                if item_chk == item_tmp:
                    tab_action_cnt = i
            except Exception as e:
                print(e)
                print('csvに入力した値が正しくありません。')

        # 別ファイルで処理実行
        time.sleep(1)
        driver.implicitly_wait(1.5)
        self.tab_action_cnt = tab_action_cnt
        buyma_actions.function(self)

if __name__ == "__main__":
    start = SeleExec()
    start.argv_chk()
    start.sele_run()