from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from datetime import datetime
import MySQLdb
import time
import sys
import csv
import re
import urllib.error
import urllib.request
import os
import math
import random


class SeleExec():
    def __init__(self):
        options = Options()
        options.add_argument('--disable-gpu')
        options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
        self.driver = webdriver.Chrome(chrome_options=options)
        self.connection = MySQLdb.connect(user='root', passwd='L3,g6ogp8>52l=K', host='localhost', db='main', charset='utf8')
        self.cursor = self.connection.cursor()
        self.argv = sys.argv

    def __del__(self):
        self.driver.close()
        self.cursor.close
        self.connection.close
        os.system('cd /var/app/ci/public/nextcloud && sudo -u apache php occ files:scan nextuser')

    def argv_chk(self):
        if len(self.argv) != 2:
            print('取得件数を指定してください。')
            sys.exit()

    def sele_run(self):
        driver = self.driver
        cursor = self.cursor
        argv = self.argv
        file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'

        try:
            cursor.execute('SELECT url FROM asos_access_urls WHERE status = 1 AND use_flag = 1 AND shop_id = 1 LIMIT 1')
            rows = cursor.fetchall()
            fix_urls = start.page_chk(argv, rows, i=0)

            for i, fix_url in enumerate(fix_urls[0]):
                driver.get(fix_url)
                time.sleep(random.randint(2, 3))
                list_links = [link.get_attribute('href') for link in driver.find_elements_by_css_selector('article > a')]
                get_cnt = start.page_chk(argv, rows, i)

                for i2, link in enumerate(list_links[0:int(get_cnt[1])]):
                    product_code = ""
                    product_price = ""
                    product_name = ""

                    try:
                        print(i2)
                        driver.get(link)
                        time.sleep(random.randint(2, 3))

                        try:
                            # セール価格の場合は定価を取得
                            product_price = driver.find_element_by_css_selector('.grid-row.rendered > span:nth-child(4)').text
                            if not product_price:
                                product_price = driver.find_element_by_css_selector('.current-price').text

                        except:
                            product_price = driver.find_element_by_css_selector('.current-price').text

                        product_code = driver.find_element_by_css_selector('.product-code > span').text
                        product_name = driver.find_element_by_css_selector('.asos-product h1').text
                        price_pnd = re.sub(r'[^\.|\d]', '', product_price)
                        list_images = [image.get_attribute('src') for image in driver.find_elements_by_css_selector('.thumbnails > ul > li > a > img')]

                    except Exception as e:
                        start.exec_error(e)
                        print('ループ途中でエラーを確認、スキップします。')
                        continue

                    print('【商品コード】' + str(product_code) + 'を取得開始')
                    price_ja_fix = start.exchange_rate(price_pnd)

                    # CSVに書き込み
                    body = [[product_code, product_name, price_ja_fix]]
                    csv_name = datetime.now().strftime("ASOS_%Y-%m-%d")
                    os.system('sudo chmod -R 777 /var/app/ci/public/nextcloud/*')
                    with open(file_path + 'Documents/' + csv_name + '.csv', 'a') as f:
                        writer = csv.writer(f)
                        writer.writerows(body)
                        time.sleep(random.randint(1, 2))

                    # 画像を保存
                    driver.execute_script("$('.gallery-image').css('visibility', '')")
                    image_src = driver.find_elements_by_css_selector('div.window > ul > li > img')
                    image_src.pop(0)

                    for i3, image in enumerate(image_src):
                        image_name = str(product_code) + '_' + str(i3) + '.jpeg'
                        print('【画像】' + image_name + 'を保存')
                        try:
                            os.system('sudo chmod -R 777 /var/app/ci/public/nextcloud/*')
                            data = urllib.request.urlopen(image.get_attribute('src')).read()
                            with open(file_path + 'Photos/' + image_name, mode='wb') as f:
                                f.write(data)

                            time.sleep(0.5)
                        except urllib.error.URLError as e:
                            print(e)

        except Exception as e:
            print('エラーが発生しました。')
            start.exec_error(e)

    def page_chk(self, argv, rows, i):
        # numでページ数を設定
        page_num = 0

        # 遷移するページ数を商品件数から算出
        if int(argv[1]) >= (72 * 1 + 1) and int(argv[1]) < (72 * 2 + 1):
            page_num = 2
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 2 + 1) and int(argv[1]) < (72 * 3 + 1):
            page_num = 3
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 3 + 1) and int(argv[1]) < (72 * 4 + 1):
            page_num = 4
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 4 + 1) and int(argv[1]) < (72 * 5 + 1):
            page_num = 5
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 5 + 1) and int(argv[1]) < (72 * 6 + 1):
            page_num = 6
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 6 + 1) and int(argv[1]) < (72 * 7 + 1):
            page_num = 7
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 7 + 1) and int(argv[1]) < (72 * 8 + 1):
            page_num = 8
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 8 + 1) and int(argv[1]) < (72 * 9 + 1):
            page_num = 9
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        elif int(argv[1]) >= (72 * 9 + 1) and int(argv[1]) < (72 * 10 + 1):
            page_num = 10
            get_cnt = int(argv[1]) - 72 * (int(page_num) - 1) if int(i) == (int(page_num) - 1) else 72
        else:
            page_num = 1
            get_cnt = argv[1]

        # ページリンクの生成
        for row in rows:
            url = row[0]
            fix_urls = []
            for i, num in enumerate(range(1, int(page_num) + 1)):
                fix_urls.append(url + '&page=' + str(num))

        return [fix_urls, get_cnt]

    def exchange_rate(self, price_pnd):
        driver = self.driver
        driver.get('https://www.currency-calc.jp/JPY_GBP')
        time.sleep(random.randint(2, 3))
        form_element = driver.find_element_by_css_selector('.calculadora-row.flex-fix > div:nth-child(1) > input')
        form_element.click()
        time.sleep(random.randint(1, 2))

        form_element.send_keys(price_pnd)
        time.sleep(random.randint(1, 2))
        driver.implicitly_wait(10)
        price_ja = driver.find_element_by_css_selector('div.currency-field-result > div').text
        price_ja = re.sub(r'\D', '', price_ja)

        price_chk = math.ceil(float(price_pnd))
        price_ja_rate = ""

        if price_chk <= 14:
            price_ja_rate = int(price_ja) * 1.6
        elif price_chk <= 30:
            price_ja_rate = int(price_ja) * 1.43
        elif price_chk <= 50:
            price_ja_rate = int(price_ja) * 1.4
        elif price_chk <= 80:
            price_ja_rate = int(price_ja) * 1.37
        elif price_chk <= 100:
            price_ja_rate = int(price_ja) * 1.35
        elif price_chk <= 150:
            price_ja_rate = int(price_ja) * 1.3
        elif price_chk <= 999999:
            price_ja_rate = int(price_ja) * 1.2
        else:
            print('計算式エラー')
        # 整数に変換
        price_ja_ceil = math.ceil(float(price_ja_rate))
        # 10の位を繰り上げ
        price_ja_fix = int(price_ja_ceil) + (100 - int(price_ja_ceil) % 100) % 100

        driver.back()
        time.sleep(random.randint(2, 3))
        return price_ja_fix

    def exec_error(self, e):
        driver = self.driver
        print(e)
        print(driver.current_url)
        driver.get_screenshot_as_file('/tmp/asos2_error_' + datetime.now().strftime("%Y-%m-%d %H:%i:%s") + '.png')
        self.cursor.execute('INSERT INTO asos_error_urls (url) VALUES (%s)', [driver.current_url])
        self.connection.commit()

start = SeleExec()
start.argv_chk()
start.sele_run()