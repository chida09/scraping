require 'selenium-webdriver'
require 'open-uri'
require 'mysql'
require 'active_support'
require 'active_support/core_ext'
require 'csv'
require 'date'

class Job
  def initialize(argv, browser='chrome')
    @home_dir = File.expand_path('..', __FILE__)
    @argv = argv
    @browser = browser
    @shop_id = 10
    @random = Random.new
    @path = '/var/app/ci/public/nextcloud/'
    @site = 'yoox'
  end

  def perform
    browser_settings(@browser)
    signin
    browser_close
  end

  def signin
    if @argv == nil
      p 'ページ数を指定してください'
      exit
    end

    chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
    1.step do |x|
      @csv_file = @path + 'data/nextuser/files/Documents/' + @site + Time.now.strftime('_%Y-%m-%d_') + x.to_s + '.csv'
      unless File.exist?(@csv_file)
        break
      end
    end

    stmt = @client.prepare('SELECT url FROM access_urls WHERE status = 1 AND use_flag = 1 AND shop_id = ? LIMIT 1')
    res = stmt.execute(@shop_id)
    res.each_with_index{|row, i|

      for num in 1..@argv.to_i do
        begin
          p "■ #{num}ページ目"
          @driver.get(row[0] + '&page=' + num.to_s)
          sleep(@random.rand(0.5)+0.5)
          product_list_url = nil
          product_list_url = @driver.current_url

          sleep 1
          # @driver.save_screenshot('/home/ruby/yoox_test.png')
          @wait.until {@driver.find_element(:css, '.img-separator > a').displayed?}
          product_links = @driver.find_elements(:css, '.img-separator > a')
          fix_product_link = []
          product_links.each_with_index{|value, i|
            fix_product_link[i] = value.attribute('href')
          }

          for num2 in 0...fix_product_link.length.to_i do
            begin

              p "#{num2+1}件目"
              @driver.navigate.to(fix_product_link[num2])
              sleep(@random.rand(1)+0.5)
              product_name = nil
              product_price = nil
              product_code = nil
              product_imgs = nil
              product_brand = nil

              @wait.until {@driver.find_element(:css, '#itemTitle > h1').displayed?}
              product_name = @driver.find_element(:css, '#itemTitle > div > a').text
              product_code_tmp = @driver.find_elements(:css, '#itemInfoCod10').size > 0 ? @driver.find_element(:css, '#itemInfoCod10').text : Time.now.strftime('%Y%m%d%H%M%S')
              product_code = @site + '_' + product_code_tmp
              product_imgs = @driver.find_elements(:css,'#itemThumbs > li > img').each.map { |e| e.attribute('src').gsub('_9_', '_12_') }
              product_price = @driver.find_elements(:css, '#item-price > span.text-secondary.text-linethrough').size > 0 ? @driver.find_element(:css, '#item-price > span.text-secondary.text-linethrough').text.delete('^0-9','^.') : @driver.find_element(:css, '#item-price > span > span:nth-child(1)').text.delete('^0-9','^.')
              product_brand = @driver.find_element(:css, '#itemTitle > h1 > a').text

              price_ja_fix = currency_conversion_usd(product_price, num2, product_list_url)
              # 画像を保存
              file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
              product_imgs.each_with_index do | imgUrl, i2 |
                chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
                image_name = "#{product_code}_#{i2}.jpeg"
                open(imgUrl) { |image|
                  File.open("#{file_path}Photos/#{image_name}", 'wb') do |file|
                    file.puts image.read
                  end
                }
              end

              # CSVに書き込み
              chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
              CSV.open(@csv_file, 'a') do |test|
                test << [product_code, product_name, price_ja_fix, product_brand]
              end
              nextcloud = `cd /var/app/ci/public/nextcloud && sudo -u apache php occ files:scan nextuser`

            rescue => e
              p e
              p @driver.current_url
              next
            end
          end
        rescue => e
          if @driver.find_elements(:css, '.itemSoldOutMessageDescription.text-center').size > 0
            sold_out = @driver.find_element(:css, '.itemSoldOutMessageDescription.text-center').text
            next if sold_out.include?('check out')
          end
          p e
          p @driver.current_url
        end
      end
    }
  end

  # USDRATE（アメリカドルを円に変換）
  def currency_conversion_usd(product_price, num2, product_list_url)
    if num2.to_i == 0 && product_list_url.include?('page=1')
      @driver.execute_script("window.open()")
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
      @driver.navigate.to 'https://www.currency-calc.jp/JPY_USD'
    else
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
    end

    sleep(@random.rand(1)+0.5)
    input_element = '.calculadora-row.flex-fix > div:nth-child(1) > input'
    @wait.until {@driver.find_element(:css, input_element).displayed?}
    @driver.find_element(:css, input_element).click
    @driver.find_element(:css, input_element).clear
    @driver.find_element(:css, input_element).send_keys(product_price)

    price_ja = @driver.find_element(:css, 'div.currency-field-result > div').text.delete('^0-9', '^.')
    price_chk = product_price.to_f.ceil
    price_ja_rate = 0

    if price_chk <= 14
      price_ja_rate = price_ja.to_f * 3
    elsif price_chk <= 30
      price_ja_rate = price_ja.to_f * 2.8
    elsif price_chk <= 50
      price_ja_rate = price_ja.to_f * 2.6
    elsif price_chk <= 80
      price_ja_rate = price_ja.to_f * 1.85
    elsif price_chk <= 100
      price_ja_rate = price_ja.to_f * 1.82
    elsif price_chk <= 150
      price_ja_rate = price_ja.to_f * 1.8
    elsif price_chk <= 999999
      price_ja_rate = price_ja.to_f * 1.78
    else
      p 為替変換エラー
      exit
    end

    price_ja_ceil = price_ja_rate.to_f.ceil
    # 10の位を繰り上げ
    price_ja_fix = price_ja_ceil.to_i + (100 - price_ja_ceil.to_i % 100) % 100

    allHandles = @driver.window_handles
    @driver.switch_to.window(allHandles[0])
    return price_ja_fix
  end

  def browser_close
    p '終了しました'
    @driver.quit
  end

  def browser_settings(browser)
    @driver = nil
    if browser=='chrome'
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
      options.add_argument('--headless')
      @driver = Selenium::WebDriver.for :chrome, options: options
      @client = Mysql::connect('127.0.0.1', 'root', 'L3,g6ogp8>52l=K', 'main')
      @client.charset = 'utf8'
      @wait = Selenium::WebDriver::Wait.new(:timeout => 30)
    end
  end
end

if __FILE__ == $0
  argv = ARGV[0]
  kj=Job.new(argv)
  kj.perform
end