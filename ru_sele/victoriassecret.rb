require 'selenium-webdriver'
require 'open-uri'
require 'mysql'
require 'active_support'
require 'active_support/core_ext'
require 'csv'
require 'date'

class Job
  def initialize(argv, browser='chrome')
    @home_dir = File.expand_path('..', __FILE__)
    @argv = argv
    @browser = browser
    @shop_id = 7
    @random = Random.new
    @site = "Victoria's Secret"
  end

  def perform
    browser_settings(@browser)
    signin
    browser_close
  end

  def signin
    begin
      if @argv == nil
        p '商品取得数を指定してください'
        exit
      end

      chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
      1.step do |x|
        @csv_file = @path + 'data/nextuser/files/Documents/' + @site + Time.now.strftime('_%Y-%m-%d_') + x.to_s + '.csv'
        unless File.exist?(@csv_file)
          break
        end
      end

      stmt = @client.prepare('SELECT url FROM access_urls WHERE status = 1 AND use_flag = 1 AND shop_id = ? LIMIT 1')
      res = stmt.execute(@shop_id)
      res.each_with_index{|row, i|
        fix_urls = page_chk(i, @argv, row)
        category_url = fix_urls[0]

        for i in 0...category_url.length do
          @driver.get category_url[i]
          sleep(@random.rand(1)+0.5)


          sleep 2000


          @wait.until {@driver.find_element(:css, '.product-image > a').displayed?}
          product_links = @driver.find_elements(:css, '.product-image > a')
          fix_product_link = []
          product_links.each_with_index{|value, i|
            fix_product_link[i] = value.attribute('href')
          }

          get_cnt = page_chk(i, @argv, row)

          for i in 0...get_cnt[1].to_i do
            @driver.navigate.to fix_product_link[i]
            sleep(@random.rand(1)+0.5)

            product_name = nil
            product_code = nil
            price_ja_fix = nil
            image_name = nil

            @wait.until {@driver.find_element(:css, 'h1.product-name').displayed?}
            product_name = @driver.find_element(:css, 'h1.product-name').text


            product_price = @driver.find_elements(:css, '.price-sales').size > 0 ? @driver.find_element(:css, '.price-sales').text.delete('^0-9','^.') : @driver.find_element(:css, '.price-standard > span').text.delete('^0-9','^.')

            product_code_tmp = @driver.find_element(:css, '.product-number').text.delete('^0-9')
            product_code = @site + '_' + product_code_tmp.to_s
            product_imgs = @driver.find_elements(:css,'.productthumbnail').each.map { |e| e.attribute('src').gsub('jpg?sw=61', 'jpg?sw=538') }

            # 画像を保存
            file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
            chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
            product_imgs.each_with_index do | imgUrl, i2 |
              image_name = "#{product_code}_#{i2}.jpeg"
              open(imgUrl) { |image|
                File.open("#{file_path}Photos/#{image_name}", 'wb') do |file|
                  file.puts image.read
                end
              }
            end
            price_ja_fix = currency_conversion_usd(product_price, i)

            # CSVに書き込み
            chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
            file_name = Time.now.strftime('modcloth_%Y-%m-%d')
            CSV.open(file_path + 'Documents/' + file_name + '.csv', 'a') do |test|
              test << [product_code, product_name, price_ja_fix, @site]
            end

            # ネクストクラウドにアップロード
            nextcloud = `cd /var/app/ci/public/nextcloud && sudo -u apache php occ files:scan nextuser`
          end
        end
      }

    rescue => e
      p e
      p @driver.current_url
    end
  end

  def page_chk(i, argv, row)
    page_param = nil

    # ページと商品数の取得
    if argv.to_i >= (306 * 1 + 1) && argv.to_i < (306 * 2 + 1)
      page_num = 1 # 306
    elsif argv.to_i >= (306 * 2 + 1) && argv.to_i < (306 * 3 + 1)
      page_num = 2 # 612
    elsif argv.to_i >= (306 * 3 + 1) && argv.to_i < (306 * 4 + 1)
      page_num = 3 # 918
    elsif argv.to_i >= (306 * 4 + 1) && argv.to_i < (306 * 5 + 1)
      page_num = 4 # 1224
    elsif argv.to_i >= (306 * 5 + 1) && argv.to_i < (306 * 6 + 1)
      page_num = 5 # 1530
    else
      page_num = 0
    end

    if page_num != 0
      get_cnt = i == page_num ? argv.to_i - (306 * page_num.to_i) : 306
    else
      get_cnt = argv
    end

    # ページリンクの生成
    fix_urls = []
    loop_cnt = page_num.to_i + 1
    for i2 in 0...loop_cnt do
      param = i2.to_i * 306
      fix_urls[i2] = row[0] + '?sz=306&start=' + param.to_s
    end

    return [fix_urls, get_cnt]
  end

  def currency_conversion_usd(product_price, i)
    # USDRATE（アメリカドルを円に変換）
    if i.to_i == 0
      @driver.execute_script("window.open()")
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
      @driver.navigate.to 'https://www.currency-calc.jp/JPY_USD'
    else
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
    end

    sleep(@random.rand(1)+0.5)
    input_element = '.calculadora-row.flex-fix > div:nth-child(1) > input'
    @wait.until {@driver.find_element(:css, input_element).displayed?}
    @driver.find_element(:css, input_element).click
    @driver.find_element(:css, input_element).clear
    @driver.find_element(:css, input_element).send_keys(product_price)

    price_ja = @driver.find_element(:css, 'div.currency-field-result > div').text.delete('^0-9', '^.')

    price_chk = product_price.to_f.ceil
    price_ja_rate = 0

    if price_chk <= 14
      price_ja_rate = price_ja.to_f * 3
    elsif price_chk <= 30
      price_ja_rate = price_ja.to_f * 2.8
    elsif price_chk <= 50
      price_ja_rate = price_ja.to_f * 2.6
    elsif price_chk <= 80
      price_ja_rate = price_ja.to_f * 1.85
    elsif price_chk <= 100
      price_ja_rate = price_ja.to_f * 1.82
    elsif price_chk <= 150
      price_ja_rate = price_ja.to_f * 1.8
    elsif price_chk <= 999999
      price_ja_rate = price_ja.to_f * 1.78
    else
      p 為替変換エラー
      exit
    end

    price_ja_ceil = price_ja_rate.to_f.ceil

    # 10の位を繰り上げ
    price_ja_fix = price_ja_ceil.to_i + (100 - price_ja_ceil.to_i % 100) % 100

    allHandles = @driver.window_handles
    @driver.switch_to.window(allHandles[0])

    return price_ja_fix
  end

  def browser_close
    @driver.quit
  end

  def browser_settings(browser)
    @driver = nil
    if browser=='chrome'
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
      options.add_argument("--user-data-dir=/home/ruby/data/victoriassecret-profile")
      options.add_argument("--password-store=detect")
      # options.add_argument('--headless')
      @driver = Selenium::WebDriver.for :chrome, options: options
      @client = Mysql::connect('127.0.0.1', 'root', 'L3,g6ogp8>52l=K', 'main')
      @client.charset = 'utf8'
      @wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    end
  end
end

if __FILE__ == $0
  argv = ARGV[0]
  kj=Job.new(argv)
  kj.perform
end