require 'selenium-webdriver'
require 'open-uri'
require 'mysql'
require 'active_support'
require 'active_support/core_ext'
require 'csv'
require 'date'

class Job
  def initialize(argv, browser='chrome')
    @home_dir = File.expand_path('..', __FILE__)
    @argv = argv
    @browser = browser
    @shop_id = 11
  end

  def perform
    browser_settings(@browser)
    signin
    browser_close
  end

  def signin
    @driver.get('https://society6.com/')
    sleep 10
    for num in 1...11 do
      fix_category_link = []
      fix_category_title = []
      sleep 1
      @driver.find_element(css: "#shop_nav > div:nth-child(#{num}) > div.desktop_styles_3xQnl > a").click
      category = @driver.find_element(css: "#shop_nav > div:nth-child(#{num}) > div.desktop_styles_3xQnl > a").text

      category_links = @driver.find_elements(:css, '#products > div > ul > li > a')
      category_links.each_with_index{|value, i|
        fix_category_link[i] = value.attribute('href')
        fix_category_title[i] = value.text

        break if value.text.blank?

        stmt = @client.prepare('INSERT INTO access_urls (url, memo, shop_id) VALUES (?,?,?)')
        stmt.execute(value.attribute('href') , + category + ' - ' + value.text ,@shop_id)
      }

      p category
      p fix_category_title
      p fix_category_link
      p "---"
    end

    p "OK"
  end

  def browser_close
    @driver.quit
  end

  def browser_settings(browser)
    @driver = nil

    if browser=='chrome'
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
      # options.add_argument('--headless')
      @driver = Selenium::WebDriver.for :chrome, options: options
      @client = Mysql::connect('127.0.0.1', 'root', 'L3,g6ogp8>52l=K', 'main')
      @client.charset = 'utf8'
        end
    @wait = Selenium::WebDriver::Wait.new(:timeout => 60)
  end
end

if __FILE__ == $0
  argv = ARGV[0]
  kj=Job.new(argv)
  kj.perform
end