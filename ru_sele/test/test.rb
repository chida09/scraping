require 'selenium-webdriver'

options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
driver = Selenium::WebDriver.for :chrome, options: options

driver.get('https://www.farfetch.com/shopping/kids/items.aspx')
sleep 1
driver.action.move_to(driver.find_element(css: '#tab_19018 > ul > li:nth-child(2) > a > span')).perform
sleep 5
driver.quit