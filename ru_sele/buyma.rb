require 'selenium-webdriver'
require 'date'
require 'csv'
require 'open-uri'
require 'active_support'
require 'active_support/core_ext'

class Job
  def initialize(csv_file, browser='chrome')
    @home_dir = File.expand_path('..', __FILE__)
    @csv_file = csv_file
    @browser = browser
    @random = Random.new
  end

  def perform
    browser_settings(@browser)
    signin
  end

  def signin
    if @csv_file == nil
      p 'csvファイルを指定してください'
      exit
    end

      file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
      chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
      csv_data = CSV.read(file_path + 'Documents/' + @csv_file, col_sep: "\t")
      @driver.get 'http://www.buyma.com/my/sell/new/'
      sleep(@random.rand(1)+0.5)

      for i in 0...csv_data.length.to_i do
        begin
          item_text = nil
          tmp_data = csv_data[i]
          data = tmp_data[0].split(',')
          p "#{i+1}件目"

          @wait.until {@driver.find_element(:css, '.bmm-c-field__input > input').displayed?}
          @driver.execute_script("$('input').css('display','')")
          @driver.execute_script("$('.sell-info-bar').css('display','none')")

          # 写真をアップロード
          for num in 0...4 do
            sleep 0.2
            begin
              chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
              @driver.find_element(:css, "input[type='file']").send_keys(file_path + 'Photos/' + data[0] + '_' + num.to_s + '.jpeg')
              break if num.to_i == 4
            rescue => e
              break
            end
          end

          # 商品名
          if data[1].length >= 60
            begin
                p "文字数オーバーです。【#{data[1]}】の文字をスペース込み半角60文字以内に変更して再出品してください。"
                exit
            rescue => e
              alert = @driver.switch_to.alert
              alert.accept
            end
          end

          @driver.find_element(:css, '.bmm-c-field__input > input').send_keys(data[1])
          # コメント
          data[3].gsub!("#", "\n") # 改行処理
          data[3].gsub!("_", "\s") # スペース処理

          @driver.find_element(:css, '.bmm-c-field__input > textarea').send_keys(data[3])

          # カテゴリ
          @driver.find_element(:css, '.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(2) > div > div.col-9 > div > div > div > div:nth-child(2) > div').location_once_scrolled_into_view

          item_text = nil
          @driver.execute_script("document.querySelectorAll('div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          @driver.find_element(:css, '#tmp').click
          sleep 0.5

          if data[4] == 'レディースファッション'
            num = 3
          elsif data[4] == 'メンズファッション'
            num = 4
          elsif data[4] == 'ベビー・キッズ'
            num = 5
          elsif data[4] == 'ビューティー'
            num = 6
          elsif data[4] == 'ライフスタイル'
            num = 7
          elsif data[4] == 'スポーツ'
            num = 8
          else
            p "カテゴリの指定が不適切です。"
            exit
            @driver.switch_to.alert.accept
          end
          pulldown_select(num)
          sleep 1

          @driver.execute_script("document.querySelectorAll('div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          @driver.find_element(:css, '#tmp').click

          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          item_text = @driver.find_element(:css, '.Select-menu-outer').text.split(/\R/)
          item_text.delete('第一カテゴリから選択') if item_text.include?('第一カテゴリから選択')

          if data[4].blank?
            p '最初のカテゴリが指定されていません。CSVを確認してください。'
            exit
            @driver.switch_to.alert.accept
          end

          @driver.execute_script("document.querySelectorAll('div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          sleep 1
          @driver.find_element(:css, '#tmp').click
          sleep 1
          @driver.find_element(:css, '#tmp').click
          sleep 1
          item_text2 = @driver.find_element(:css, '.Select-menu-outer').text.split(/\R/)
          item_text2.each_with_index{|value, num|
            if value == data[5]
              if num >= 11 # 調整
                num = num.to_i + 1
              end
              pulldown_select(num)
            end
          }

          if @driver.find_elements(:css, 'form > div:nth-child(3) > div:nth-child(1) > div > div.col-9 > div > div > div.bmm-c-field__input > div > div:nth-child(3) > div > div').size > 0
            @driver.execute_script("document.querySelectorAll('div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
            @driver.find_element(:css, '#tmp').click
            @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
            item_text = @driver.find_element(:css, '.Select-menu-outer').text.split(/\R/)
            item_text.each_with_index{|value, num|
              pulldown_select(num) if value == data[6]
            }
          end

          # ブランド名
          sleep 1
          @driver.find_element(:css, 'form > div:nth-child(3) > div:nth-child(4) > div').location_once_scrolled_into_view
          if data[61].blank?
            @driver.execute_script("$(\"input[type='checkbox']\").css('display','block')")
            sleep 0.5
            @driver.find_element(:css, 'div > div > div > div.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(2) > div > div.col-9 > div > div > div > div > label > input').click
          else
            @driver.find_element(:css, '.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(2) > div > div.col-9 > div > div > div > div:nth-child(1) > div > div > div.grid-noBottom > div > div > div > div > input').send_keys(data[61])
            sleep 0.5
            @driver.action.send_keys(:tab).perform
          end
          sleep 1

          # シーズン
          if @driver.find_elements(:css, '.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(5) > div > div.col-9 > div > div > p > a').size > 0
            @driver.find_element(:css, '.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(5) > div > div.col-9 > div > div > p > a').location_once_scrolled_into_view
          else
            # モデルの設定がある場合は非表示にする
            @driver.execute_script("document.querySelectorAll('div.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(3) > div:nth-child(3)')[0].style.display = 'none';")
            sleep 0.5
          end

          @driver.execute_script("document.querySelectorAll('.col-9 > div > div > div > div > div > span.Select-multi-value-wrapper > div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          @wait.until {@driver.find_element(:css, '#tmp').displayed?}

          @driver.find_element(:css, '#tmp').click
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          sleep 0.5

          item_text = @driver.find_element(:css, '.Select-menu-outer').text.split(/\R/)
          item_text.each_with_index{|value, num|
            pulldown_select(num) if value == data[7]
          }

          @driver.find_element(:css, '.bmm-l-cnt__body.sell-body.is-btnbar-fixed > form > div:nth-child(4) > div:nth-child(1) > div > div.col-3 > div > p').location_once_scrolled_into_view
          # テーマ
          @driver.execute_script("document.querySelectorAll('.col-9 > div > div > div > div > div > div > div > span.Select-multi-value-wrapper > div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          @driver.execute_script("$('.Select-menu-outer').css('display','none')")
          @wait.until {@driver.find_element(:css, '#tmp').displayed?}
          @driver.find_element(:css, '#tmp').click
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          sleep(@random.rand(0.5)+0.5)
          item_text_tmp = @driver.find_element(:css, '.Select-menu-outer').text.split(/\R/)
          item_text_tmp.delete('PUSH')　if item_text_tmp.include?('PUSH')

          item_text = item_text_tmp.each_with_index.reject{ |value, num| num % 2 != 0 }
          item_text.each_with_index{|value, num|
            pulldown_select(num) if value[0] == data[8]
          }

          # 色の系統1
          @driver.find_element(:css, '.col-9 > div > div > div.bmm-c-field > div.bmm-c-field__input > textarea').location_once_scrolled_into_view
          @driver.execute_script("document.querySelectorAll('.sell-color-option__name')[0].innerHTML = '<select id=tmp></select>';")
          @wait.until {@driver.find_element(:css, '#tmp').displayed?}
          @driver.find_element(:css, '#tmp').click
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
          item_text.each_with_index{|value, num|
            pulldown_select(num.to_i + 1) if value == data[9]
          }

          @driver.find_element(:css, '.bmm-c-form-table__body > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > input').send_keys(data[10])

          # 色の系統2
          unless data[11].blank?
            @driver.find_element(:link_text, '新しい色を追加').click
            @driver.execute_script("document.querySelectorAll('.sell-color-option__name')[1].innerHTML = '<select id=tmp></select>';")
            @wait.until {@driver.find_element(:css, '#tmp').displayed?}
            @driver.find_element(:css, '#tmp').click
            @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
            item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
            item_text.each_with_index{|value, num|
              pulldown_select(num) if value == data[11]
            }
            @driver.find_element(:css, '.bmm-c-form-table__body > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > input').send_keys(data[12])
          end

          # 色の系統3
          unless data[13].blank?
              @driver.find_element(:link_text, '新しい色を追加').click
              @driver.execute_script("document.querySelectorAll('.sell-color-option__name')[2].innerHTML = '<select id=tmp></select>';")
              @wait.until {@driver.find_element(:css, '#tmp').displayed?}
              @driver.find_element(:css, '#tmp').click
              @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
              item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
              item_text.each_with_index{|value, num|
                pulldown_select(num) if value == data[13]
              }
              @driver.find_element(:css, '.bmm-c-form-table__body > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > input').send_keys(data[14])
          end

          @driver.find_element(:link_text, '色サイズの設定方法').send_keys(:tab)
          @driver.action.send_keys(:down).send_keys(:tab).send_keys(:tab).send_keys(:enter).perform

          if data[15] == "バリエーションあり"
            @driver.action.send_keys(:down).send_keys(:enter).perform
            sleep(@random.rand(0.5)+0.5)
            @driver.action.send_keys(:tab).send_keys(:enter).perform
            @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
            item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
            item_text.each_with_index{|value, num|
              pulldown_select(num) if value == data[17]
            }

            for num in 1...4 do
              size_name = nil
              japan_size = nil

              begin
                if num.to_i == 1
                  size_name = data[18]
                  japan_size = data[19]
                elsif num.to_i == 2
                  size_name = data[26]
                  japan_size = data[27]
                else
                  size_name = data[34]
                  japan_size = data[35]
                end
              rescue => e
                  # p 'サイズ未選択'
                  next
              end

              # サイズ名
              @driver.find_element(:css, ".bmm-c-form-table__body > table > tbody > tr:nth-child(#{num}) > td:nth-child(2) > div > div > div > input").send_keys(size_name)

              # 参考日本サイズ
              @driver.action.send_keys(:tab).send_keys(:enter).perform
              @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
              item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
              item_text.each_with_index{|value, num|
                pulldown_select(num) if value == japan_size
              }

              # 編集
              @driver.find_element(:css, ".bmm-c-form-table__body > table > tbody > tr:nth-child(#{num}) > td:nth-child(4) > div > p > a").click
              sleep(@random.rand(0.5)+0.5)

              for num2 in 1...7 do
                begin
                  if num.to_i == 1
                    @driver.find_element(:css, ".sell-size-detail-form > div:nth-child(#{num2}) > div > input").send_keys(data[19 + num2.to_i])
                  elsif num.to_i == 2
                    @driver.find_element(:css, ".sell-size-detail-form > div:nth-child(#{num2}) > div > input").send_keys(data[27 + num2.to_i])
                  else
                    @driver.find_element(:css, ".sell-size-detail-form > div:nth-child(#{num2}) > div > input").send_keys(data[35 + num2.to_i])
                  end
                rescue => e
                  next
                end
              end
              # 設定完了
              @driver.find_element(:css, '.bmm-c-modal__btns > button.bmm-c-btn.bmm-c-btn--p').click

              if num <= 2
                @driver.find_element(:link_text, '新しいサイズを追加').click
              end
            end
          else
            # バリエーション無し
            @driver.action.send_keys(:enter).perform
            @driver.action.send_keys(:tab).send_keys(:enter).perform
            if data[16] == 'ONE SIZE'
                @driver.action.send_keys(:down).send_keys(:enter).perform
            else
                @driver.action.send_keys(:enter).perform
            end

            # 日本参考サイズ
            @driver.action.send_keys(:tab).send_keys(:enter).perform
            @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
            item_text = @driver.find_element(:css, '.Select-menu-outer').text.split()
            item_text.each_with_index{|value, num|
              pulldown_select(num) if value == data[19]
            }

            # サイズ編集（指定ありの場合ののみ）
            sleep(@random.rand(0.5)+0.5)
            if @driver.find_elements(:css, ".bmm-c-form-table__body > table > tbody > tr > td:nth-child(3) > div > p > a").size > 0
              @driver.find_element(:css, ".bmm-c-form-table__body > table > tbody > tr > td:nth-child(3) > div > p > a").click
              begin
                for num in 1...7 do
                  @driver.find_element(:css, ".sell-size-detail-form > div:nth-child(#{num}) > div > input").send_keys(data[19 + num.to_i])
                end
              rescue => e
                  # p 'サイズ詳細未入力'
              end
              # 設定完了
              @driver.find_element(:css, '.bmm-c-modal__btns > button.bmm-c-btn.bmm-c-btn--p').click
            end
          end

          # 色・サイズ補足情報
          data[42].gsub!("#", "\n") # 改行処理
          data[42].gsub!("_", "\s") # スペース処理
          @driver.find_element(:css, 'div:nth-child(1) > div > div.col-9 > div > div > div.bmm-c-field > div.bmm-c-field__input > textarea').send_keys(data[42])

          # 販売可否/在庫
          @driver.execute_script("$(\"input[type='checkbox']\").css('display','block')")
          @driver.find_element(:css, '.col-9 > div > div > div > label > input').location_once_scrolled_into_view

          for num in 1...10 do
            begin
              if data[42 + num.to_i] == 'なし'
                @driver.find_element(:css, ".bmm-c-form-table__body > table > tbody > tr:nth-child(#{num}) > td:nth-child(3) > label").click
              end
            rescue => e
              next
            end
          end

          # 数量
          @driver.find_element(:css, '.col-9 > div > div > div:nth-child(2) > div > div.grid-noBottom > div > div > div > div > input').send_keys(data[52])

          # 連絡事項
          if data[53] == '必須にしない'
            @driver.find_element(:link_text, '配送方法追加').location_once_scrolled_into_view
            @driver.find_element(:css, 'form > div:nth-child(4) > div:nth-child(4) > div > div.col-9 > div > div > div > label > input').click
          end

          # 配送方法
          sleep(@random.rand(0.5)+0.5)
          @driver.find_element(:css, '.react-datepicker-wrapper > div > input').location_once_scrolled_into_view
          @driver.execute_script("$('.bmm-c-checkbox--pointer-none').css('pointer-events','auto')")
          @driver.execute_script("$('.bmm-c-checkbox__input').css('display', 'block')")

          delivery = data[54].split('・')
          delivery.each_with_index{|value, num|
            if value == 'ゆうパック'
              element_num = 1
            elsif value == '宅急便コンパクト'
              element_num = 10
            elsif value == 'ネコポス'
              element_num = 5
            elsif value == '宅急便'
              element_num = 4
            else
              element_num = nil
            end

            unless element_num.blank?
              @driver.find_element(:css, ".bmm-c-form-table__body > table > tbody > tr:nth-child(#{element_num}) > td.bmm-c-form-table__icon-cell > label > input").click
            end
          }

          # 購入期限
          kigen = '.col-9 > div > div > div > div > div > div > div > div.react-datepicker-wrapper > div > input'
          @driver.find_element(:css, '.col-9 > div > div > div > div > div.bmm-c-field__input > input').location_once_scrolled_into_view
          @driver.execute_script("document.querySelectorAll('#{kigen}')[0].setAttribute('onfocus', 'this.select()')")
          @driver.find_element(:css, kigen).click
          @driver.find_element(:css, kigen).send_keys(:delete)
          @driver.find_element(:css, kigen).send_keys(data[55])
          @driver.find_element(:css, kigen).send_keys(:tab)

          # 買付地
          @driver.execute_script("$(\"input[type='radio']\").css('display','block')")
          @driver.find_element(:css, '.col-9 > div > div > div > div > div.bmm-c-field__input > input').location_once_scrolled_into_view

          # 前回の登録情報をリセット
          @driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[0].click();")

          # 調整
          @driver.action.send_keys(:enter).perform
          @driver.action.send_keys(:up).send_keys(:up).perform
          @driver.action.send_keys(:enter).perform
          @driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[1].click();")

          sleep(@random.rand(0.5)+0.5)
          @driver.execute_script("document.querySelectorAll('.col-9 > div > div:nth-child(2) > div > div:nth-child(1) > div > div > span.Select-multi-value-wrapper > div.Select-value > span')[0].innerHTML = '<select id=tmp></select>';")
          @wait.until {@driver.find_element(:css, '#tmp').displayed?}
          @driver.find_element(:css, '#tmp').click
          sleep 1
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          item_text1 = @driver.find_element(:css, '.Select-menu-outer').text.split()

          sleep 1
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          item_text1 = @driver.find_element(:css, '.Select-menu-outer').text.split()

          # この時点で初期化出来ていないと上手く出来ない
          item_text1.each_with_index{|value, num|
            sleep(@random.rand(1)+0.5)
            unless value.blank?
              pulldown_select(num) if value == data[56]
            end
          }
          sleep(@random.rand(1)+0.5)

          @driver.execute_script("document.querySelectorAll('.col-9 > div > div:nth-child(2) > div > div:nth-child(2) > div > div > span.Select-multi-value-wrapper > div.Select-value > span')[0].innerHTML = '<select id=tmp></select>';")
          @driver.find_element(:css, '#tmp').click
          sleep(@random.rand(0.5)+0.5)
          item_text2 = @driver.find_element(:css, '.Select-menu-outer').text.split()

          item_text2.each_with_index{|value, num|
            sleep(@random.rand(1)+0.5)
            unless value.blank?
              pulldown_select(num) if value == data[57]
            end
          }

          # 調整
          @driver.action.send_keys(:enter).perform
          @driver.action.send_keys(:up).perform
          @driver.action.send_keys(:enter).perform
          @driver.action.send_keys(:enter).perform
          @driver.action.send_keys(:down).perform
          @driver.action.send_keys(:enter).perform

          unless data[58].blank?
            if data[58] != '選択なし'
              @driver.execute_script("document.querySelectorAll('.col-9 > div > div:nth-child(2) > div > div:nth-child(3) > div > div > span.Select-multi-value-wrapper > div.Select-value > span')[0].innerHTML = '<select id=tmp></select>';")
              @wait.until {@driver.find_element(:css, '#tmp').displayed?}
              @driver.find_element(:css, '#tmp').click
              @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
              sleep(@random.rand(0.5)+0.5)
              item_text3 = @driver.find_element(:css, '.Select-menu-outer').text.split()
              item_text3.each_with_index{|value, num|
                unless value.blank?
                  pulldown_select(num) if value == data[58]
                end
              }
            end
          end

          # 買付地/海外で固定 ここまで
          kakaku = '.bmm-c-field__input > div > div.col-4 > div > div > input'
          @driver.find_element(:css, kakaku).location_once_scrolled_into_view
          sleep 0.5

          # 発送地/国内で固定
          @driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[2].click();")

          if @driver.find_elements(:css, ' div.Select-placeholder').size > 0
            @driver.execute_script("document.querySelectorAll('div.Select-placeholder')[0].innerHTML = '<select id=tmp></select>';")
          else
            @driver.execute_script("document.querySelectorAll('div:nth-child(4) > div > div.col-9 > div > div:nth-child(2) > div > div > div > div > span.Select-multi-value-wrapper > div.Select-value > span')[0].innerHTML = '<select id=tmp></select>';")
          end

          @wait.until {@driver.find_element(:css, '#tmp').displayed?}
          @driver.find_element(:css, '#tmp').click

          sleep 0.5
          @wait.until {@driver.find_element(:css, '.Select-menu-outer').displayed?}
          sleep(@random.rand(0.5)+0.5)
          pref_text = @driver.find_element(:css, '.Select-menu-outer').text.split()

          pulldown_select(num=13)
          @driver.action.send_keys(:enter).perform
          sleep(@random.rand(1)+0.5)

          # 価格
          @driver.find_element(:css, kakaku).send_keys(data[2])

          # 参考価格
          unless data[59].blank?
            @driver.execute_script("document.getElementsByClassName('bmm-c-radio__body')[5].click();")
            @driver.find_element(:css, '.col-9 > div > div > div:nth-child(2) > div > div > div > div > input').send_keys(data[59])
          end

          # 出品メモ
          unless data[60].blank?
            @driver.find_element(:css, 'form > div:nth-child(8) > div > div > div.col-9 > div > div > div.bmm-c-field__input > input').send_keys(data[60])
          end

          # 入力内容を確認する
          @driver.find_element(:css, '.sell-btnbar > div > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--m.bmm-c-btn--thick').click

          @wait.until {@driver.find_element(:css, '.bmm-c-modal__btns > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--l').displayed?}
          # 出品する
          @driver.find_element(:css, '.bmm-c-modal__btns > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--l').click

          # 続けて出品する
          @wait.until {@driver.find_element(:css, '.sell-complete__btns > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--l').displayed?}
          @driver.find_element(:css, '.sell-complete__btns > button.bmm-c-btn.bmm-c-btn--p.bmm-c-btn--l').click

        rescue => e
          p "操作エラー：#{data[1]}"
          p e
          @driver.navigate.refresh
          sleep 1
          @driver.switch_to.alert.accept
          @driver.execute_script("window.scrollTo(0, 0);")
          next
        end
      end
    end

    def pulldown_select(num)
      if num.to_i == 0
        @driver.action.send_keys(:enter).perform
      elsif num.to_i == 1
        @driver.action.send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 2
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 3
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 4
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 5
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 6
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 7
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 8
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 9
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 10
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 11
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 12
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 13
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 14
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 15
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 16
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 17
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 18
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 19
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 20
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 21
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 22
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 23
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 24
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      elsif num.to_i == 25
        @driver.action.send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:down).send_keys(:enter).perform
      else
        p "プルダウン選択エラー"
        exit
      end
    end

  def browser_settings(browser)
    @driver = nil

    if browser=='chrome'
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
      options.add_argument('--user-data-dir=/home/ruby/data/north_2')
      options.add_argument('--password-store=detect')
      @driver = Selenium::WebDriver.for :chrome, options: options
      @wait = Selenium::WebDriver::Wait.new(:timeout => 30)
    end
  end
end

if __FILE__ == $0
  csv_file = ARGV[0]
  kj=Job.new(csv_file)
  kj.perform
end