require 'selenium-webdriver'
require 'open-uri'
require 'mysql'
require 'active_support'
require 'active_support/core_ext'
require 'csv'
require 'date'

class Job
  def initialize(argv, browser='chrome')
    @home_dir = File.expand_path('..', __FILE__)
    @argv = argv
    @browser = browser
    @shop_id = 1
    @random = Random.new
  end

  def perform
    browser_settings(@browser)
    signin
    browser_close
  end

  def signin
    stmt = @client.prepare('SELECT url FROM product_add_urls WHERE status = 1 AND shop_id = ?')
    res = stmt.execute(@shop_id)
    res.each_with_index{|row, i|
      begin
        @driver.get(row[0])
        sleep(@random.rand(1)+0.5)

        product_name = nil
        product_code = nil
        price_ja_fix = nil
        image_name = nil


        @wait.until {@driver.find_element(:css, 'ul > li:nth-child(1) > div.product-info > a > div.product-title').displayed?}
        product_name1 = @driver.find_element(:css, 'ul > li:nth-child(1) > div.product-info > a > div.product-title').text
        product_name2 = @driver.find_element(:css, 'ul > li:nth-child(2) > div.product-info > a > div.product-title').text

        product_price1 = @driver.find_element(:css, 'ul > li:nth-child(1) > div > a > div.product-price > div.grid-row.rendered > span:nth-child(4)').text.delete('^0-9', '^.')
        if product_price1.blank?
          product_price1 = @driver.find_element(:css, 'ul > li:nth-child(1) > div.product-info > a > div.product-price > div > span.current-price').text.delete('^0-9', '^.')
        end

        product_price2 = @driver.find_element(:css, 'ul > li:nth-child(2) > div.product-info > a > div.product-price > div.grid-row.rendered > span:nth-child(4)').text.delete('^0-9', '^.')
        if product_price2.blank?
          product_price2 = @driver.find_element(:css, 'ul > li:nth-child(2) > div.product-info > a > div.product-price > div > span.current-price').text.delete('^0-9', '^.')
        end

        p_code_tmp = @driver.find_elements(:css, '.product-code > span').size > 0 ? @driver.find_element(:css, '.product-code > span').text : Time.now.strftime('%Y%m%d%H%M%S')
        product_code = "asos_#{p_code_tmp}"

        @driver.execute_script("$('.gallery-image').css('visibility', '')")
        product_imgs = @driver.find_elements(:css, '.product-gallery > div.thumbnails > ul > li > a > img')
        product_imgs.shift

        # 画像を保存
        file_path = '/var/app/ci/public/nextcloud/data/nextuser/files/'
        chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
        product_imgs.each_with_index do | imgUrl, i2 |
          image_name = "#{product_code}_#{i2}.jpeg"
          open(imgUrl.attribute('src')) { |image|
            File.open("#{file_path}Photos/#{image_name}", 'wb') do |file|
              file.puts image.read
            end
          }
        end
        price_ja_fix = currency_conversion_pnd(product_price1, product_price2, i)

        # CSVに書き込み
        chmod = `sudo chmod -R 777 /var/app/ci/public/nextcloud/*`
        file_name = Time.now.strftime('asos_add_%Y-%m-%d')
        CSV.open(file_path + 'Documents/' + file_name + '.csv', 'a') do |test|
          test << [product_code, product_name1, price_ja_fix[0]]
          test << [product_code, product_name2, price_ja_fix[1]]
        end

        # ネクストクラウドにアップロード
        nextcloud = `cd /var/app/ci/public/nextcloud && sudo -u apache php occ files:scan nextuser`

        stmt = @client.prepare('UPDATE product_add_urls SET status=? WHERE url=?')
        stmt.execute(2, @driver.current_url)

      rescue => e
        p e
        p @driver.current_url
        next
      end
    }
  end

  def browser_close
    @driver.quit
  end

    # PNDRATE（ポンドを円に変換）
    def currency_conversion_pnd(product_price1, product_price2, i)
    if i.to_i == 0
      @driver.execute_script("window.open()")
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
      @driver.navigate.to 'https://www.currency-calc.jp/JPY_GBP'
    else
      last_window = @driver.window_handles.last
      @driver.switch_to.window(last_window)
    end

    for i2 in 0...2 do
      product_price_res = i2.to_i == 0 ? product_price1 : product_price2

      sleep(@random.rand(1)+0.5)
      input_element = '.calculadora-row.flex-fix > div:nth-child(1) > input'
      @wait.until {@driver.find_element(:css, input_element).displayed?}
      @driver.find_element(:css, input_element).click
      @driver.find_element(:css, input_element).clear
      @driver.find_element(:css, input_element).send_keys(product_price_res)

      price_ja = @driver.find_element(:css, 'div.currency-field-result > div').text.delete('^0-9', '^.')

      price_chk = product_price_res.to_f.ceil
      price_ja_rate = 0

      price_ja_rate = price_ja.to_f * 1.6
      if price_chk <= 14
        price_ja_rate = price_ja.to_f * 1.6
      elsif price_chk <= 30
        price_ja_rate = price_ja.to_f * 1.43
      elsif price_chk <= 50
        price_ja_rate = price_ja.to_f * 1.4
      elsif price_chk <= 80
        price_ja_rate = price_ja.to_f * 1.37
      elsif price_chk <= 100
        price_ja_rate = price_ja.to_f * 1.35
      elsif price_chk <= 150
        price_ja_rate = price_ja.to_f * 1.3
      elsif price_chk <= 999999
        price_ja_rate = price_ja.to_f * 1.2
      else
        p 為替変換エラー
        exit
      end
      price_ja_ceil = price_ja_rate.to_f.ceil
      # 10の位を繰り上げ
      price_ja_fix = price_ja_ceil.to_i + (100 - price_ja_ceil.to_i % 100) % 100

      if i2.to_i == 0
        price_ja_fix1 = price_ja_fix
      else
        price_ja_fix2 = price_ja_fix
      end
    end

    allHandles = @driver.window_handles
    @driver.switch_to.window(allHandles[0])

    return [price_ja_fix1, price_ja_fix2]
  end

  def browser_settings(browser)
    @driver = nil
    if browser=='chrome'
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument('--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7')
      options.add_argument('--headless')
      @driver = Selenium::WebDriver.for :chrome, options: options
      @client = Mysql::connect('127.0.0.1', 'root', 'L3,g6ogp8>52l=K', 'main')
      @client.charset = 'utf8'
      @wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    end
  end
end

if __FILE__ == $0
  argv = ""
  kj=Job.new(argv)
  kj.perform
end